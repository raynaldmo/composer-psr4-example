Composer PSR-4 Example
======================
Example project demonstrating using Composer PSR-4 autoloading

### References
* [How Composer Autoloads PHP Files](https://jinoantony.com/blog/how-composer-autoloads-php-files)
* [PSR-4 Autoloading example with Composer](http://joequery.me/code/composer-psr4-example/)  


### Install project
```$ composer install```

### Run application
```$ php -S localhost:8000 app.php```

* Point browser to: localhost:8000