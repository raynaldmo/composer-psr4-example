<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Composer PSR-4 Autoload Example</title>
</head>
<body>
<h1>Composer PSR-4 Autoload Example</h1>

<?php # index.php
error_reporting(E_ALL);
ini_set('display_errors', 1);

echo '<p style="color:green;">Start PHP</p>';

require __DIR__ . '/vendor/autoload.php';
// require_once 'src/Department.php';
// require_once 'src/Employee.php';

// Use 'use' operator for aliasing class names
use App\Company\Department;
use App\Company\Employee;

// Create a department:
$hr = new Department('Executive');

// Create employees:
$e1 = new Employee('Raynald Mompoint');
$e2 = new Employee('Jacqueline Clay');

// Add the employees to the department:
$hr->addEmployee($e1);
$hr->addEmployee($e2);

;

// Delete the objects:
// unset($hr, $e1, $e2);

?>

</body>
</html>